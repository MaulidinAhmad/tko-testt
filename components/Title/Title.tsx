import { ReactElement } from "react";
import styles from "./Title.module.css";

interface TitleInterface {
  text: string;
}

const Title: React.FC<TitleInterface> = ({ text }) => {
  return <h1 className={styles.title}>{text}</h1>;
};

export default Title;
