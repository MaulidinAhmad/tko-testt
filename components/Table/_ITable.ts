import { ReactComponentElement, ReactElement } from "react";
import { UserInterface } from "../../redux/user/type";

export interface Key {
  name: string;
  title: string;
}

export interface ActionInterface {
  [key: string]: {
    action: (data: any) => void;
    render: ReactElement;
  };
}

export interface TableInterface<T> {
  data: T[];
  keydata: Key[];
  handleEdit?: (id: number) => void;
  handleDelete?: (id: number) => void;
}
