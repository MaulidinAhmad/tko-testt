import { TableInterface } from "./_ITable";
import Head from "./_Header";
import Body from "./_Body";
import styles from "./Table.module.css";
import { PropsWithChildren } from "react";

const Table = <T extends { id: number; [x: string]: any }>({
  data,
  keydata,
  handleEdit,
  handleDelete,
}: PropsWithChildren<TableInterface<T>>) => {
  return (
    <div className={styles.table}>
      <Head
        handleEdit={handleEdit}
        handleDelete={handleDelete}
        keyHead={keydata}
      />
      <Body<T>
        handleEdit={handleEdit}
        handleDelete={handleDelete}
        keyBody={keydata}
        data={data}
      />
    </div>
  );
};

export default Table;
