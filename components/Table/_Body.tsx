import { PropsWithChildren, useEffect } from "react";
import { UserInterface } from "../../redux/user/type";
import { Key, ActionInterface } from "./_ITable";
import styles from "./Table.module.css";
import { useRouter } from "next/router";
import Link from "next/link";

interface TbodyInterface<T> {
  data: T[];
  keyBody: Key[];
  handleEdit?: (id: number) => void;
  handleDelete?: (id: number) => void;
}

const _Body = <T extends { id: number; [x: string]: any }>({
  data,
  keyBody,
  handleEdit,
  handleDelete,
}: PropsWithChildren<TbodyInterface<T>>) => {
  const router = useRouter();

  return (
    <ul className={styles.tbody}>
      {data.map((val, index: number) => {
        return (
          <li key={index}>
            <ul className={styles.tbodyItem}>
              {keyBody.map((_val, index: number) => {
                return <li key={index}>{val[_val.name]}</li>;
              })}

              <li>
                {handleEdit && (
                  <Link href={router.pathname + "/" + val.id}>
                    <button>Edit</button>
                  </Link>
                )}
                {handleDelete && (
                  <button onClick={() => handleDelete(val.id)}>Delete</button>
                )}
              </li>
            </ul>
          </li>
        );
      })}
    </ul>
  );
};

export default _Body;
