import { Key, ActionInterface } from "./_ITable";
import styles from "./Table.module.css";

interface TheadTinterface {
  keyHead: Key[];
  handleEdit?: (id: number) => void;
  handleDelete?: (id: number) => void;
}

const _Header = ({ keyHead, handleEdit }: TheadTinterface) => {
  return (
    <ul className={styles.thead}>
      {keyHead.map((val, index: number) => (
        <li key={index}>{val.title}</li>
      ))}
      <li>Action</li>
    </ul>
  );
};

export default _Header;
