import { useRouter } from "next/router";
import React from "react";
import styles from "./sidebar.module.css";
import Link from "next/link";

const Sidebar: React.FC = () => {
  const router = useRouter();

  return (
    <nav className={styles.container}>
      <h1 className={styles.logo}>LOGO</h1>
      <ul>
        <Link href="/user">
          <li
            className={router.pathname.startsWith("/user") ? styles.active : ""}
          >
            <img src="/icons/user-icon.svg" alt="user-icon" />
            <a>User</a>
          </li>
        </Link>
        <Link href="/bankaccount">
          <li
            className={
              router.pathname.startsWith("/bankaccount") ? styles.active : ""
            }
          >
            <img src="/icons/bank-icon.svg" alt="user-icon" />
            <a>Bank Account</a>
          </li>
        </Link>
      </ul>
    </nav>
  );
};

export default Sidebar;
