import { ReactElement } from "react";
import styles from "./Button.module.css";

interface ButtonInterface {
  children: any;
  [x: string]: any;
}

const Button: React.FC<ButtonInterface> = (props) => {
  return (
    <button className={styles.button} {...props}>
      {props.children}
    </button>
  );
};

export default Button;
