import { ReactElement } from "react";
import styles from "./Card.module.css";

interface CardInterface {
  children: ReactElement | null | undefined;
}

const Card: React.FC<CardInterface> = ({ children }) => {
  return <div className={styles.card}>{children}</div>;
};

export default Card;
