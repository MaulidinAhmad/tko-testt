import styles from "./Alert.module.css";

const Alert: React.FC<{ message: string; success?: boolean }> = ({
  message,
  success,
}) => {
  return (
    <div
      className={`${styles.alert} ${success ? styles.success : styles.error}`}
    >
      {message}
    </div>
  );
};

export default Alert;
