import Sidebar from "../Sidebar/Sidebar";
import Head from "next/head";
import styles from "../../styles/Home.module.css";
import { ReactElement } from "react";

interface Layout {
  children: ReactElement;
}

const Layout: React.FC<Layout> = ({ children }) => {
  return (
    <div className={styles.container}>
      <Head>
        <title>TKO TEST</title>
        <meta
          name="description"
          content="a test web for join tokocrypto team"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Sidebar />

      <main className={styles.main}>{children}</main>
    </div>
  );
};

export default Layout;
