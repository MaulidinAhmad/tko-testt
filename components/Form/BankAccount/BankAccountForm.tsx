import { Formik } from "formik";
import * as Yup from "yup";

// Component
import styles from "../form.module.css";
import Input from "../Input/TextInput";
import Button from "../../Button/Button";

// Redux
import { BankAccountInterface } from "../../../redux/bankaccount/type";

interface BankAccountFormInterface {
  data?: BankAccountInterface | null;
  onSubmit: (data: any, { setSubmitting, resetForm }: any) => void;
}

const Schema = Yup.object().shape({
  firstname: Yup.string().required(),
  bankname: Yup.string().required(),
  lastname: Yup.string().required(),
  accountbank: Yup.number().required().min(10),
});

const BankAccountForm: React.FC<BankAccountFormInterface> = ({
  onSubmit,
  data,
}) => {
  return (
    <Formik
      validationSchema={Schema}
      onSubmit={(values, { setSubmitting, resetForm }) =>
        onSubmit(values, { setSubmitting, resetForm })
      }
      initialValues={{
        firstname: data?.firstname || "",
        lastname: data?.lastname || "",
        bankname: data?.bankname || "",
        accountbank: data?.accountbank || "",
      }}
      enableReinitialize={true}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        touched,
        errors,
        values,
      }) => (
        <form className={styles.form} onSubmit={handleSubmit}>
          <Input
            label="First Name"
            type="text"
            name="firstname"
            value={values.firstname || ""}
            onBlur={handleBlur}
            onChange={handleChange}
            errorText={
              (errors.firstname && touched.firstname && errors.firstname) ||
              undefined
            }
          />

          <Input
            label="Last Name"
            type="text"
            name="lastname"
            value={values.lastname || ""}
            onBlur={handleBlur}
            onChange={handleChange}
            errorText={
              (errors.lastname && touched.lastname && errors.lastname) ||
              undefined
            }
          />

          <Input
            label="Bank Name"
            type="text"
            name="bankname"
            value={values.bankname || ""}
            onBlur={handleBlur}
            onChange={handleChange}
            errorText={
              (errors.bankname && touched.bankname && errors.bankname) ||
              undefined
            }
          />

          <Input
            label="Account Bank"
            type="text"
            name="accountbank"
            value={values.accountbank || ""}
            onBlur={handleBlur}
            onChange={handleChange}
            errorText={
              (errors.accountbank &&
                touched.accountbank &&
                errors.accountbank) ||
              undefined
            }
          />

          <Button type="submit">SUBMIT</Button>
        </form>
      )}
    </Formik>
  );
};

export default BankAccountForm;
