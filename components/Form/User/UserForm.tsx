import { Formik } from "formik";
import * as Yup from "yup";

// Component
import styles from "../form.module.css";
import Input from "../Input/TextInput";
import Button from "../../Button/Button";

// Redux
import { UserInterface } from "../../../redux/user/type";

interface UserFormInterface {
  data?: UserInterface | null;
  onSubmit: (data: any, { setSubmitting, resetForm }: any) => void;
}

const Schema = Yup.object().shape({
  name: Yup.string().required(),
  email: Yup.string().email().required(),
  address: Yup.string().required(),
  phone: Yup.string().required(),
});

const UserForm: React.FC<UserFormInterface> = ({ onSubmit, data }) => {
  return (
    <Formik
      validationSchema={Schema}
      onSubmit={(values, { setSubmitting, resetForm }) =>
        onSubmit(values, { setSubmitting, resetForm })
      }
      initialValues={{
        name: data?.name || "",
        email: data?.email || "",
        address: data?.address || "",
        phone: data?.phone || "",
      }}
      enableReinitialize={true}
    >
      {({
        handleSubmit,
        handleChange,
        handleBlur,
        touched,
        errors,
        values,
      }) => (
        <form className={styles.form} onSubmit={handleSubmit}>
          <Input
            label="Name"
            type="text"
            name="name"
            value={values.name || ""}
            onBlur={handleBlur}
            onChange={handleChange}
            errorText={
              (errors.name && touched.name && errors.name) || undefined
            }
          />

          <Input
            label="Email"
            type="email"
            name="email"
            value={values.email || ""}
            onBlur={handleBlur}
            onChange={handleChange}
            errorText={
              (errors.email && touched.email && errors.email) || undefined
            }
          />

          <Input
            label="Address"
            type="text"
            name="address"
            value={values.address || ""}
            onBlur={handleBlur}
            onChange={handleChange}
            errorText={
              (errors.address && touched.address && errors.address) || undefined
            }
          />

          <Input
            label="Phone"
            type="text"
            name="phone"
            value={values.phone || ""}
            onBlur={handleBlur}
            onChange={handleChange}
            errorText={
              (errors.phone && touched.phone && errors.phone) || undefined
            }
          />

          <Button type="submit">SUBMIT</Button>
        </form>
      )}
    </Formik>
  );
};

export default UserForm;
