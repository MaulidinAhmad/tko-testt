import styles from "./TextInput.module.css";

interface TextInput {
  label: string;
  name: string;
  errorText?: string;
  [x: string]: any;
}

const TextInput: React.FC<TextInput> = (props) => {
  return (
    <div className={styles.textInput}>
      <label htmlFor={props.name}>{props.label}</label>
      <input id={props.name} {...props} />
      {props.errorText && (
        <span className={styles.textError}>{props.errorText}</span>
      )}
    </div>
  );
};

export default TextInput;
