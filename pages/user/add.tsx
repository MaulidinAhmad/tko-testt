// Component Import
import Title from "../../components/Title/Title";
import Card from "../../components/Card/Card";
import UserForm from "../../components/Form/User/UserForm";
import { createUser } from "../../redux/user/action";

// Redux Import
import { useDispatch } from "react-redux";

import { useState } from "react";
import Alert from "../../components/Alert/Alert";

const UserAdd = () => {
  const dispatch = useDispatch();
  const [success, setsuccess] = useState<string | null>(null);
  const [error, seterror] = useState<string | null>(null);

  const handleSubmit = async (
    values: any,
    { setSubmitting, resetForm }: any
  ) => {
    try {
      await dispatch(createUser(values));
      setsuccess("Successfully Add Bank Account Data!!");
      seterror(null);
      setSubmitting(false);
      resetForm();
    } catch (error: any) {
      setSubmitting(false);
      setsuccess(null);
      seterror(typeof error === "string" ? error : error?.message || "");
    }
    console.log(values);
  };
  return (
    <>
      <Title text="User Add" />
      <Card>
        <>
          {(error || success) && (
            <Alert
              message={error || success || ""}
              success={success ? true : false}
            />
          )}
          <UserForm
            onSubmit={(values: any, { setSubmitting, resetForm }: any) =>
              handleSubmit(values, { setSubmitting, resetForm })
            }
          />
        </>
      </Card>
    </>
  );
};

export default UserAdd;
