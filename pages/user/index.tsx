// Core Import
import { NextPage } from "next";
import Link from "next/link";
import { useRouter } from "next/router";
import { useEffect } from "react";

// Component Import
import Card from "../../components/Card/Card";
import Title from "../../components/Title/Title";
import Table from "../../components/Table/Table";
import Button from "../../components/Button/Button";

// Redux Import
import { useSelector, useDispatch } from "react-redux";
import { AppState } from "../../redux/root-reducer";
import { UserInterface } from "../../redux/user/type";
import { getAllUser, deleteUser } from "../../redux/user/action";

const UserPage: NextPage = ({}) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const users: UserInterface[] = useSelector(
    (state: AppState) => state.user.users
  );

  useEffect(() => {
    if (!users || users.length === 0) dispatch(getAllUser());
  }, []);

  const handleDelete = (id: number) => {
    dispatch(deleteUser(id));
  };

  return (
    <>
      <Title text="User" />
      <Card>
        <>
          <Link href={router.pathname + "/" + "add"}>
            <Button>Add User</Button>
          </Link>
          <Table<UserInterface>
            keydata={[
              { name: "id", title: "Id" },
              { name: "name", title: "Name" },
              { name: "email", title: "Email" },
              { name: "phone", title: "Phone" },
              { name: "address", title: "Address" },
            ]}
            data={users}
            handleEdit={() => {}}
            handleDelete={(id) => handleDelete(id)}
          ></Table>
        </>
      </Card>
    </>
  );
};

export default UserPage;
