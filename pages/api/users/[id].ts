import { usersRepo } from "../../../repository/user.repository";

import type { NextApiRequest, NextApiResponse } from "next";
export default handler;

function handler(req: NextApiRequest, res: NextApiResponse) {
  switch (req.method) {
    case "GET":
      return getUserById();
    case "PUT":
      return updateUser();
    case "DELETE":
      return deleteUser();
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`);
  }

  function getUserById() {
    if (typeof req.query.id === "string") {
      const user = usersRepo.getById(parseInt(req.query.id));
      return res.status(200).json(user);
    }
  }

  function updateUser() {
    try {
      if (typeof req.query.id === "string") {
        usersRepo.update(parseInt(req.query.id), req.body);
        return res.status(200).json({});
      }
    } catch (error) {
      return res.status(400).json({ message: error });
    }
  }

  function deleteUser() {
    if (typeof req.query.id === "string") {
      usersRepo.delete(parseInt(req.query.id));
      return res.status(200).json({});
    }
  }
}
