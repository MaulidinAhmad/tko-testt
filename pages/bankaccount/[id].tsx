// Core Import
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

// Component Import
import Title from "../../components/Title/Title";
import Card from "../../components/Card/Card";
import BankAccountForm from "../../components/Form/BankAccount/BankAccountForm";

// Redux Import
import { useDispatch } from "react-redux";
import { UpdateBankAccount } from "../../redux/bankaccount/action";
import { BankAccountInterface } from "../../redux/bankaccount/type";

//  Service
import { bankaccountService } from "../../services/bankaccount.service";
import Alert from "../../components/Alert/Alert";

const BankAccount = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { id } = router.query;
  const [data, setdata] = useState<BankAccountInterface | null>(null);
  const [success, setsuccess] = useState<string | null>(null);
  const [error, seterror] = useState<string | null>(null);

  const handleGetData = async (id: number) => {
    try {
      const result = await bankaccountService.getById(id);
      setdata(result);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (typeof id === "string") handleGetData(parseInt(id));
  }, [id]);

  const handleSubmit = async (
    values: any,
    { setSubmitting, resetForm }: any
  ) => {
    try {
      const { id } = router.query;
      if (typeof id === "string") {
        await dispatch(UpdateBankAccount(parseInt(id), values));
        setsuccess("Successfully Update Bank Account Data!!");
        seterror(null);
      }
      setSubmitting(false);
    } catch (error: any) {
      setSubmitting(false);
      setsuccess(null);
      seterror(typeof error === "string" ? error : error?.message || "");
    }
  };

  return (
    <>
      <Title text="User Edit" />
      <Card>
        <>
          {(error || success) && (
            <Alert
              message={error || success || ""}
              success={success ? true : false}
            />
          )}
          <BankAccountForm
            data={data}
            onSubmit={(data, { setSubmitting, resetForm }) =>
              handleSubmit(data, { setSubmitting, resetForm })
            }
          />
        </>
      </Card>
    </>
  );
};

export default BankAccount;
