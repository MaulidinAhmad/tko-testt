// Component Import
import Title from "../../components/Title/Title";
import Card from "../../components/Card/Card";
import BankAccountForm from "../../components/Form/BankAccount/BankAccountForm";
import { createBankAccount } from "../../redux/bankaccount/action";
import Alert from "../../components/Alert/Alert";

// Redux Import
import { useDispatch } from "react-redux";
import { useState } from "react";

const BankAccountAdd = () => {
  const dispatch = useDispatch();
  const [success, setsuccess] = useState<string | null>(null);
  const [error, seterror] = useState<string | null>(null);

  const handleSubmit = async (
    values: any,
    { setSubmitting, resetForm }: any
  ) => {
    try {
      await dispatch(createBankAccount(values));
      setsuccess("Successfully Add Bank Account Data!!");
      seterror(null);
      setSubmitting(false);
      resetForm();
    } catch (error: any) {
      console.log(error, "Error");
      setSubmitting(false);
      setsuccess(null);
      seterror(typeof error === "string" ? error : error?.message || "");
    }
  };
  return (
    <>
      <Title text="User Add" />
      <Card>
        <>
          {(error || success) && (
            <Alert
              message={error || success || ""}
              success={success ? true : false}
            />
          )}
          <BankAccountForm
            onSubmit={(values: any, { setSubmitting, resetForm }: any) =>
              handleSubmit(values, { setSubmitting, resetForm })
            }
          />
        </>
      </Card>
    </>
  );
};

export default BankAccountAdd;
