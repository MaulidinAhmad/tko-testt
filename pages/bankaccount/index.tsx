// Core Import
import { NextPage } from "next";
import { useEffect } from "react";
import Link from "next/link";
import { useRouter } from "next/router";

// Component Import
import Card from "../../components/Card/Card";
import Title from "../../components/Title/Title";
import Table from "../../components/Table/Table";
import Button from "../../components/Button/Button";

// Redux Import
import { useSelector, useDispatch } from "react-redux";
import { AppState } from "../../redux/root-reducer";
import { BankAccountInterface } from "../../redux/bankaccount/type";
import {
  getAllBankAccount,
  deleteBankAccount,
} from "../../redux/bankaccount/action";

const BankAccountPage: NextPage = ({}) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const bankaccount: BankAccountInterface[] = useSelector(
    (state: AppState) => state.bankaccount.accountData
  );

  useEffect(() => {
    if (!bankaccount || bankaccount.length === 0) dispatch(getAllBankAccount());
  }, []);

  // delete handle function
  const handleDelete = (id: number) => {
    dispatch(deleteBankAccount(id));
  };

  return (
    <>
      <Title text="Bank Account" />
      <Card>
        <>
          <Link href={router.pathname + "/" + "add"}>
            <Button>Add Bank Account</Button>
          </Link>
          <Table<BankAccountInterface>
            keydata={[
              { name: "id", title: "Id" },
              { name: "bankname", title: "Bank Name" },
              { name: "firstname", title: "First Name" },
              { name: "lastname", title: "Last Name" },
              { name: "accountbank", title: "Bank Account" },
            ]}
            data={bankaccount}
            handleEdit={() => {}}
            handleDelete={(id) => handleDelete(id)}
          ></Table>
        </>
      </Card>
    </>
  );
};

export default BankAccountPage;
