import { apiUrl } from "../config/api";
import { fetchWrapper } from "../helpers/fetch-wrapper";

export const bankaccountService = {
  getAll,
  getById,
  create,
  update,
  delete: _delete,
};

const baseUrl = `${apiUrl}/bankaccount`;

function getAll() {
  return fetchWrapper.get(baseUrl);
}

function getById(id: number) {
  return fetchWrapper.get(`${baseUrl}/${id}`);
}

function create(params: any) {
  return fetchWrapper.post(baseUrl, params);
}

function update(id: number, params: any) {
  return fetchWrapper.put(`${baseUrl}/${id}`, params);
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete(id: number) {
  return fetchWrapper.delete(`${baseUrl}/${id}`);
}
