import { BankAccountInterface } from "../redux/bankaccount/type";
const fs = require("fs");
let bankaccountdata: BankAccountInterface[] = require("../data/bankaccountdata.json");

export const bankAccountRepo = {
  getAll,
  getById,
  create,
  update,
  delete: _delete,
};

function getAll() {
  return bankaccountdata;
}

function getById(id: number) {
  return bankaccountdata.find(
    (x: BankAccountInterface) => x.id.toString() === id.toString()
  );
}

function create({
  accountbank,
  bankname,
  lastname,
  firstname,
  id,
}: BankAccountInterface) {
  const bankaccount: BankAccountInterface = {
    accountbank,
    bankname,
    lastname,
    firstname,
    id,
  };

  // validate
  if (bankaccountdata.find((x) => x.accountbank === bankaccount.accountbank))
    throw `Bank Account with the accountbank ${bankaccount.accountbank} already exists`;

  // generate new bankaccount id
  bankaccount.id = bankaccountdata.length
    ? Math.max(...bankaccountdata.map((x) => x.id)) + 1
    : 1;

  // set date created and updated
  bankaccount.dateCreated = new Date().toISOString();
  bankaccount.dateUpdated = new Date().toISOString();

  // add and save bankaccount
  bankaccountdata.push(bankaccount);
  saveData();
  return bankaccount;
}

function update(
  id: number,
  { accountbank, bankname, lastname, firstname }: BankAccountInterface
) {
  const params = { accountbank, bankname, lastname, firstname };
  const bankaccount = bankaccountdata.find(
    (x) => x.id.toString() === id.toString()
  );

  if (!bankaccount) {
    throw `Bank Account Not Exist!!`;
  }

  // validate
  if (
    params.accountbank !== bankaccount.accountbank &&
    bankaccountdata.find((x) => x.accountbank === params.accountbank)
  )
    throw `Bank Account with the accountbank ${params.accountbank} already exists`;

  // set date updated
  bankaccount.dateUpdated = new Date().toISOString();

  // update and save
  Object.assign(bankaccount, params);
  saveData();
}

// prefixed with underscore '_' because 'delete' is a reserved word in javascript
function _delete(id: number) {
  // filter out deleted user and save
  bankaccountdata = bankaccountdata.filter(
    (x) => x.id.toString() !== id.toString()
  );
  saveData();
}

// private helper functions

function saveData() {
  //   fs.writeFileSync(
  //     "data/bankaccountdata.json",
  //     JSON.stringify(bankaccountdata, null, 4)
  //   );
}
