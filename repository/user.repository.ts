import { UserInterface } from "../redux/user/type";
const fs = require("fs");
let users: UserInterface[] = require("../data/userdata.json");

export const usersRepo = {
  getAll,
  getById,
  create,
  update,
  delete: _delete,
};

function getAll() {
  return users;
}

function getById(id: number) {
  return users.find((x: UserInterface) => x.id.toString() === id.toString());
}

function create({ email, address, name, id, phone }: UserInterface) {
  const user: UserInterface = { email, address, name, id, phone };

  // validate
  if (users.find((x) => x.email === user.email))
    throw `User with the email ${user.email} already exists`;

  // generate new user id
  user.id = users.length ? Math.max(...users.map((x) => x.id)) + 1 : 1;

  // set date created and updated
  user.dateCreated = new Date().toISOString();
  user.dateUpdated = new Date().toISOString();

  // add and save user
  users.push(user);
  saveData();
  return user;
}

function update(id: number, { email, address, name, phone }: UserInterface) {
  const params = { email, address, name, phone };
  const user = users.find((x) => x.id.toString() === id.toString());

  if (!user) {
    throw `User Not Exist!!`;
  }

  // validate
  if (
    params.email !== user.email &&
    users.find((x) => x.email === params.email)
  )
    throw `User with the email ${params.email} already exists`;

  // set date updated
  user.dateUpdated = new Date().toISOString();

  // update and save
  Object.assign(user, params);
  saveData();
}

// prefixed with underscore '_' because 'delete' is a reserved word in javascript
function _delete(id: number) {
  // filter out deleted user and save
  users = users.filter((x) => x.id.toString() !== id.toString());
  saveData();
}

// private helper functions

function saveData() {
  // fs.writeFileSync("data/userdata.json", JSON.stringify(users, null, 4));
}
