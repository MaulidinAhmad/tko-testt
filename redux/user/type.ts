export const GET_USERS = "GET_USERS";
export const ADD_USER = "ADD_USER";
export const UPDATE_USER = "UPDATE_USER";
export const DELETE_USER = "DELETE_USER";

export interface User {
  users: UserInterface[];
}

type TUserInterface = {
  [key: string]: string | number | null | undefined;
};

export interface UserInterface extends TUserInterface {
  id: number;
  name: string;
  email: string;
  address: string;
  phone: string;
  dateCreated?: string;
  dateUpdated?: string;
}

export interface GetUsers {
  type: typeof GET_USERS;
  payload: UserInterface[];
}

export interface AddUser {
  type: typeof ADD_USER;
  payload: any;
}

export interface DeleteUser {
  type: typeof DELETE_USER;
  payload: any;
}

export interface UpdateUser {
  type: typeof UPDATE_USER;
  payload: any;
}

export type Action = GetUsers | AddUser | DeleteUser | UpdateUser;
