import {
  User,
  Action,
  GET_USERS,
  ADD_USER,
  DELETE_USER,
  UPDATE_USER,
} from "./type";

const INITIAL_STATE: User = {
  users: [],
};

export default (state = INITIAL_STATE, action: Action) => {
  switch (action.type) {
    case GET_USERS:
      return {
        ...state,
        users: action.payload,
      };
    case ADD_USER:
      return {
        ...state,
        users: [...state.users, action.payload],
      };
    case UPDATE_USER:
      return {
        ...state,
        users: action.payload,
      };
    case DELETE_USER:
      return {
        ...state,
        users: action.payload,
      };
    default:
      return state;
  }
};
