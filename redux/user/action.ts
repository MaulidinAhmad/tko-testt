import { throws } from "assert";
import { Dispatch } from "redux";
import { userService } from "../../services/user.service";
import {
  GET_USERS,
  UPDATE_USER,
  ADD_USER,
  DELETE_USER,
  UserInterface,
} from "./type";

export const getAllUser = () => async (dispatch: Dispatch) => {
  try {
    const data = await userService.getAll();
    dispatch({
      type: GET_USERS,
      payload: data,
    });
  } catch (error) {
    throw error;
  }
};

export const createUser = (data: any) => async (dispatch: Dispatch) => {
  try {
    const result = await userService.create(data);

    dispatch({
      type: ADD_USER,
      payload: result,
    });
  } catch (error) {
    throw error;
  }
};

export const UpdateUser =
  (id: number, params: any) => async (dispatch: Dispatch, getState: any) => {
    try {
      const users = getState().user.users;

      const notUpdatedData = users.filter(
        (x: UserInterface) => x.id.toString() !== id.toString()
      );

      const user = users.find(
        (x: UserInterface) => x.id.toString() === id.toString()
      );

      const newUserData = { ...user, ...params };
      const newData = [...notUpdatedData, newUserData];
      newData.sort((a: UserInterface, b: UserInterface) => a.id - b.id);

      await userService.update(id, params);

      dispatch({
        type: UPDATE_USER,
        payload: newData,
      });
    } catch (error) {
      throw error;
    }
  };

export const deleteUser =
  (id: number) => async (dispatch: Dispatch, getState: any) => {
    try {
      const usersData = getState().user.users;

      await userService.delete(id);
      const newData = usersData.filter((val: any) => {
        return val.id !== id;
      });

      dispatch({
        type: DELETE_USER,
        payload: newData,
      });
    } catch (error) {}
  };
