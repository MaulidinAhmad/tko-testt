import { combineReducers } from "redux";

import user from "./user/reducer";
import bankaccount from "./bankaccount/reducer";

const rootReducer = combineReducers({
  user,
  bankaccount,
});

export type AppState = ReturnType<typeof rootReducer>;
export default rootReducer;
