import { Dispatch } from "redux";
import { bankaccountService } from "../../services/bankaccount.service";
import {
  GET_BANK_ACCOUNT,
  UPDATE_BANK_ACCOUNT,
  ADD_BANK_ACCOUNT,
  DELETE_BANK_ACCOUNT,
  BankAccountInterface,
} from "./type";

export const getAllBankAccount = () => async (dispatch: Dispatch) => {
  try {
    const data = await bankaccountService.getAll();
    dispatch({
      type: GET_BANK_ACCOUNT,
      payload: data,
    });
  } catch (error) {
    throw error;
  }
};

export const createBankAccount = (data: any) => async (dispatch: Dispatch) => {
  try {
    const result = await bankaccountService.create(data);

    dispatch({
      type: ADD_BANK_ACCOUNT,
      payload: result,
    });
  } catch (error) {
    throw error;
  }
};

export const UpdateBankAccount =
  (id: number, params: any) => async (dispatch: Dispatch, getState: any) => {
    try {
      const bankaccount = getState().bankaccount.accountData;

      const notUpdatedData = bankaccount.filter(
        (x: BankAccountInterface) => x.id.toString() !== id.toString()
      );

      const user = bankaccount.find(
        (x: BankAccountInterface) => x.id.toString() === id.toString()
      );

      const newUserData = { ...user, ...params };
      const newData = [...notUpdatedData, newUserData];
      newData.sort(
        (a: BankAccountInterface, b: BankAccountInterface) => a.id - b.id
      );

      await bankaccountService.update(id, params);

      dispatch({
        type: UPDATE_BANK_ACCOUNT,
        payload: newData,
      });
    } catch (error) {
      throw error;
    }
  };

export const deleteBankAccount =
  (id: number) => async (dispatch: Dispatch, getState: any) => {
    try {
      const usersData = getState().bankaccount.accountData;

      await bankaccountService.delete(id);
      const newData = usersData.filter((val: any) => {
        return val.id !== id;
      });

      dispatch({
        type: DELETE_BANK_ACCOUNT,
        payload: newData,
      });
    } catch (error) {
      throw error;
    }
  };
