import {
  BankAccount,
  Action,
  GET_BANK_ACCOUNT,
  ADD_BANK_ACCOUNT,
  DELETE_BANK_ACCOUNT,
  UPDATE_BANK_ACCOUNT,
} from "./type";

const INITIAL_STATE: BankAccount = {
  accountData: [],
};

export default (state = INITIAL_STATE, action: Action) => {
  switch (action.type) {
    case GET_BANK_ACCOUNT:
      return {
        ...state,
        accountData: action.payload,
      };
    case ADD_BANK_ACCOUNT:
      return {
        ...state,
        accountData: [...state.accountData, action.payload],
      };

    case UPDATE_BANK_ACCOUNT:
      return {
        ...state,
        accountData: action.payload,
      };
    case DELETE_BANK_ACCOUNT:
      return {
        ...state,
        accountData: action.payload,
      };
    default:
      return state;
  }
};
