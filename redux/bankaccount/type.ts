export const GET_BANK_ACCOUNT = "GET_BANK_ACCOUNT";
export const ADD_BANK_ACCOUNT = "ADD_BANK_ACCOUNT";
export const UPDATE_BANK_ACCOUNT = "UPDATE_BANK_ACCOUNT";
export const DELETE_BANK_ACCOUNT = "DELETE_BANK_ACCOUNT";

export interface BankAccount {
  accountData: BankAccountInterface[];
}

type TBankAccountInterface = {
  [key: string]: string | number | null | undefined;
};

export interface BankAccountInterface extends TBankAccountInterface {
  id: number;
  bankname: string;
  firstname: string;
  lastname: string;
  accountbank: string;
  dateCreated?: string;
  dateUpdated?: string;
}

export interface GetBanlAccount {
  type: typeof GET_BANK_ACCOUNT;
  payload: BankAccountInterface[];
}

export interface AddBankAccount {
  type: typeof ADD_BANK_ACCOUNT;
  payload: any;
}

export interface DeleteBankAccount {
  type: typeof DELETE_BANK_ACCOUNT;
  payload: any;
}

export interface UpdateBankAccount {
  type: typeof UPDATE_BANK_ACCOUNT;
  payload: any;
}

export type Action =
  | GetBanlAccount
  | AddBankAccount
  | DeleteBankAccount
  | UpdateBankAccount;
